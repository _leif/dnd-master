//
//  NSView+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 4/26/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import Cocoa

extension NSView {
    func setBackgroundColor(color: NSColor){
        if self is NSButton {
            (self as! NSButton).isBordered = false
        }
        self.wantsLayer = true
        self.layer?.backgroundColor = color.cgColor
    }
    func setBorder(color: NSColor, width: CGFloat){
        self.layer?.borderColor = color.cgColor
        self.layer?.borderWidth = width
    }
}

