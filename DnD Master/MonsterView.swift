//
//  MonsterView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/4/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit

struct MonsterModel : Carryable, Fightable, Levelable, Living{
    //Living{
    var health : Int
    var currentHealth : Int
    //Levelable{
    var level : Int
    var xp : Int
    //Fightable
    var initiative : Int
    var ac : Int
    var speed : Int
    var stats : Stats
    //Carryable
    var currency : Currency
}


class MonsterView: SKShapeNode, Placeable{
    var placeableType: PlaceableType = PlaceableType.monster
    
    var nodeName : String
    var size : CGSize
    var color : SKColor? {
        didSet {
            fillColor = color!
            strokeColor = color!
        }
    }
    var monsterModel : MonsterModel
    
    init(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Level level: Int, Xp xp: Int, Initiative initiative: Int, Ac ac: Int, Speed speed: Int, Stats stats: Stats, Currency currency: Currency) {
        monsterModel = MonsterModel(health: health, currentHealth: currentHealth, level: level, xp: xp, initiative: initiative, ac: ac, speed: speed, stats: stats, currency: currency)
        nodeName = name
        self.size = size
        super.init()
        self.color = color
        path = CGPath(rect: CGRect( x: 0, y: 0, width: size.width, height: size.height), transform: nil)
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
