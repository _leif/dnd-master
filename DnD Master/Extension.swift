//
//  Extension.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/27/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import SpriteKit

//TODO: Break this down! LINES: 173
func changePlaceable(string: String, tag: Int, object: Placeable){
    var textFieldTag : TextFieldTag = TextFieldTag.name
    for t in 0...20{
        if(t == tag){
            textFieldTag = TextFieldTag(rawValue: t)!
        }
    }
    
    //change data based on textfield and object passed types
    if(object is PlayerView){
        let placeableOfType = object as! PlayerView
        switch (textFieldTag){
        case .name:
            placeableOfType.nodeName = string
        case .color:
            let integer = UInt64(string, radix: 16)
            let color = SKColor.hexColor(rgbValue: Int(integer!))
            placeableOfType.color = color
        case .level:
            placeableOfType.playerModel.level = string.toInt(defaultInt: 0)
        case .xp:
            placeableOfType.playerModel.xp = string.toInt(defaultInt: 0)
        case .speed:
            placeableOfType.playerModel.speed = string.toInt(defaultInt: 0)
        case .ac:
            placeableOfType.playerModel.ac = string.toInt(defaultInt: 0)
        case .inititative:
            placeableOfType.playerModel.initiative = string.toInt(defaultInt: 1)
        case .str:
            placeableOfType.playerModel.stats.strength = string.toInt(defaultInt: 1)
        case .con:
            placeableOfType.playerModel.stats.constitution = string.toInt(defaultInt: 1)
        case .dex:
            placeableOfType.playerModel.stats.dexterity = string.toInt(defaultInt: 1)
        case .int:
            placeableOfType.playerModel.stats.intelligence = string.toInt(defaultInt: 1)
        case .wis:
            placeableOfType.playerModel.stats.wisdom = string.toInt(defaultInt: 1)
        case .cha:
            placeableOfType.playerModel.stats.charisma = string.toInt(defaultInt: 1)
        case .pp:
            placeableOfType.playerModel.currency.pp = string.toInt(defaultInt: 0)
        case .gp:
            placeableOfType.playerModel.currency.gp = string.toInt(defaultInt: 0)
        case .ep:
            placeableOfType.playerModel.currency.ep = string.toInt(defaultInt: 0)
        case .sp:
            placeableOfType.playerModel.currency.sp = string.toInt(defaultInt: 0)
        case .cp:
            placeableOfType.playerModel.currency.cp = string.toInt(defaultInt: 0)
        case .HP:
            placeableOfType.playerModel.health = string.toInt(defaultInt: 0)
        case .currentHP:
            placeableOfType.playerModel.currentHealth = string.toInt(defaultInt: 0)
        default:
            return
        }
    }else if(object is MonsterView){
        let placeableOfType = object as! MonsterView
        switch (textFieldTag){
        case .name:
            placeableOfType.nodeName = string
        case .color:
            let integer = UInt64(string, radix: 16)
            let color = SKColor.hexColor(rgbValue: Int(integer!))
            placeableOfType.color = color
        case .level:
            placeableOfType.monsterModel.level = string.toInt(defaultInt: 0)
        case .xp:
            placeableOfType.monsterModel.xp = string.toInt(defaultInt: 0)
        case .speed:
            placeableOfType.monsterModel.speed = string.toInt(defaultInt: 0)
        case .ac:
            placeableOfType.monsterModel.ac = string.toInt(defaultInt: 0)
        case .inititative:
            placeableOfType.monsterModel.initiative = string.toInt(defaultInt: 1)
        case .str:
            placeableOfType.monsterModel.stats.strength = string.toInt(defaultInt: 1)
        case .con:
            placeableOfType.monsterModel.stats.constitution = string.toInt(defaultInt: 1)
        case .dex:
            placeableOfType.monsterModel.stats.dexterity = string.toInt(defaultInt: 1)
        case .int:
            placeableOfType.monsterModel.stats.intelligence = string.toInt(defaultInt: 1)
        case .wis:
            placeableOfType.monsterModel.stats.wisdom = string.toInt(defaultInt: 1)
        case .cha:
            placeableOfType.monsterModel.stats.charisma = string.toInt(defaultInt: 1)
        case .pp:
            placeableOfType.monsterModel.currency.pp = string.toInt(defaultInt: 0)
        case .gp:
            placeableOfType.monsterModel.currency.gp = string.toInt(defaultInt: 0)
        case .ep:
            placeableOfType.monsterModel.currency.ep = string.toInt(defaultInt: 0)
        case .sp:
            placeableOfType.monsterModel.currency.sp = string.toInt(defaultInt: 0)
        case .cp:
            placeableOfType.monsterModel.currency.cp = string.toInt(defaultInt: 0)
        case .HP:
            placeableOfType.monsterModel.health = string.toInt(defaultInt: 0)
        case .currentHP:
            placeableOfType.monsterModel.currentHealth = string.toInt(defaultInt: 0)
        default:
            return
        }
    }else if(object is NPCView){
        let placeableOfType = object as! NPCView
        switch (textFieldTag){
        case .name:
            placeableOfType.nodeName = string
        case .color:
            let integer = UInt64(string, radix: 16)
            let color = SKColor.hexColor(rgbValue: Int(integer!))
            placeableOfType.color = color
        case .pp:
            placeableOfType.npcModel.currency.pp = string.toInt(defaultInt: 0)
        case .gp:
            placeableOfType.npcModel.currency.gp = string.toInt(defaultInt: 0)
        case .ep:
            placeableOfType.npcModel.currency.ep = string.toInt(defaultInt: 0)
        case .sp:
            placeableOfType.npcModel.currency.sp = string.toInt(defaultInt: 0)
        case .cp:
            placeableOfType.npcModel.currency.cp = string.toInt(defaultInt: 0)
        case .HP:
            placeableOfType.npcModel.health = string.toInt(defaultInt: 0)
        case .currentHP:
            placeableOfType.npcModel.currentHealth = string.toInt(defaultInt: 0)
        default:
            return
        }
    }else if(object is ObjectView){
        let placeableOfType = object as! ObjectView
        switch (textFieldTag){
        case .name:
            placeableOfType.nodeName = string
        case .color:
            let integer = UInt64(string, radix: 16)
            let color = SKColor.hexColor(rgbValue: Int(integer!))
            placeableOfType.color = color
        case .pp:
            placeableOfType.objectModel.currency.pp = string.toInt(defaultInt: 0)
        case .gp:
            placeableOfType.objectModel.currency.gp = string.toInt(defaultInt: 0)
        case .ep:
            placeableOfType.objectModel.currency.ep = string.toInt(defaultInt: 0)
        case .sp:
            placeableOfType.objectModel.currency.sp = string.toInt(defaultInt: 0)
        case .cp:
            placeableOfType.objectModel.currency.cp = string.toInt(defaultInt: 0)
        default:
            return
        }
    }else if(object is RadiusView){
        let placeableOfType = object as! RadiusView
        switch(textFieldTag){
        case .name:
            placeableOfType.nodeName = string
        case .color:
            let integer = UInt64(string, radix: 16)
            let color = SKColor.hexColor(rgbValue: Int(integer!))
            placeableOfType.color = color
        case .size:
            if let n = NumberFormatter().number(from: string) {
                let f = CGFloat(n)
                print(placeableOfType.size)
                //could be a little better fit
                placeableOfType.size = .init(width: (f*256)-64, height: (f*256)-64)
            }
        default:
            return
        }
    }
}





