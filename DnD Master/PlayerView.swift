//
//  PlayerView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/4/17.
//  Copyright © 2017 oneleif. All rights reserved.
//


import Foundation
import SpriteKit
//user input and functions
//prepare to display
struct PlayerModel : Carryable, Fightable, Levelable, Living{
    //Living
    var health : Int
    var currentHealth : Int
    //Levelable
    var level : Int
    var xp : Int
    //Fightable
    var initiative : Int
    var ac : Int
    var speed : Int
    var stats : Stats
    //Carryable
    var currency : Currency
    
}

class PlayerView: SKShapeNode, Placeable{
    var placeableType: PlaceableType = PlaceableType.player
    var nodeName : String
    var size : CGSize
    var color : SKColor? {
        didSet {
            fillColor = color!
            strokeColor = color!
        }
    }
    var playerModel : PlayerModel
    
    init(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Level level: Int, Xp xp: Int, Initiative initiative: Int, Ac ac: Int, Speed speed: Int, Stats stats: Stats, Currency currency: Currency) {
        playerModel = PlayerModel(health: health, currentHealth: currentHealth, level: level, xp: xp, initiative: initiative, ac: ac, speed: speed, stats: stats, currency: currency)
        nodeName = name
        self.size = size
        super.init()
        self.color = color
        path = CGPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerWidth: size.width/2, cornerHeight: size.height/2, transform: nil)
        self.position = position
    }
    init(position: CGPoint) {
        playerModel = PlayerModel(health: 0, currentHealth: 0, level: 1, xp: 0, initiative: 0, ac: 0, speed: 0, stats: Stats(strength: 0, dexterity: 0, constitution: 0, intelligence: 0, wisdom: 0, charisma: 0 ), currency: Currency(pp: 0, gp: 0, ep: 0, sp: 0, cp: 0))
        nodeName = "???"
        self.size = CGSize(width: 25, height: 25)
        super.init()
        self.color = .black
        path = CGPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerWidth: size.width/2, cornerHeight: size.height/2, transform: nil)
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
