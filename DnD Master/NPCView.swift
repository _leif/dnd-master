//
//  NPCView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/4/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit

struct NPCModel : Carryable, Living{
    //Living{
    var health : Int
    var currentHealth : Int
    //Carryable
    var currency : Currency
}


class NPCView: SKShapeNode, Placeable{
    var placeableType: PlaceableType =  PlaceableType.npc
    
    var nodeName : String
    var size : CGSize
    var color : SKColor? {
        didSet {
            fillColor = color!
            strokeColor = color!
        }
    }
    
    var npcModel : NPCModel
    
    init(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Currency currency: Currency) {
        nodeName = name
        self.size = size
        npcModel = NPCModel(health: health, currentHealth: currentHealth, currency: currency)
        super.init()
        self.color = color
        path = CGPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerWidth: size.width/2, cornerHeight: size.height/2, transform: nil)
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
