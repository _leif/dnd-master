//
//  ObjectView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/4/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit

struct ObjectModel : Carryable{
    //Carryable
    var currency : Currency
}

class ObjectView: SKShapeNode, Placeable{
    var placeableType: PlaceableType =  PlaceableType.object
    var nodeName : String
    var size : CGSize
    var color : SKColor? {
        didSet {
            fillColor = color!
            strokeColor = color!
        }
    }
    var objectModel : ObjectModel
    
    init(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Currency currency: Currency) {
        objectModel = ObjectModel(currency: currency)
        nodeName = name
        self.size = size
        super.init()
        self.color = color
        path = CGPath(rect: CGRect( x: 0, y: 0, width: size.width, height: size.height), transform: nil)
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
