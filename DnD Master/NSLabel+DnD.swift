//
//  NSLabel+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 4/26/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import Cocoa

class NSLabel: NSTextField {
    init(frame: NSRect, text: String) {
        super.init(frame: frame)
        stringValue = text
        isBezeled = false
        drawsBackground = false
        isEditable = false
        isSelectable = false
    }
    
    func setText(str: String){
        stringValue = str
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
