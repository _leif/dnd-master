//
//  SKNode+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 3/23/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import SpriteKit

enum DNDZ: CGFloat {
    case radius = 2
    
}

extension SKNode{
    func addRadius(Position pos: CGPoint, Color color: AppKit.NSColor, Radius radius: CGFloat, Scene scene: SKScene){
        let circle = SKShapeNode(circleOfRadius: radius)
        var tempPos = pos
        tempPos.x = tempPos.x + 64
        tempPos.y = tempPos.y + 64
        circle.position = tempPos
        circle.strokeColor = color
        circle.glowWidth = 6
        circle.fillColor = .clear
        circle.zPosition = DNDZ.radius.rawValue
        scene.addChild(circle)
    }
}
