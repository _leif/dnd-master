//
//  GameScene.swift
//  DnD Master
//
//  Created by Zach Eriksen on 2/21/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import SpriteKit
import GameplayKit

@available(OSX 10.12.2, *)
class GameScene: SKScene, NSTextFieldDelegate {
    //TODO: DNDCameraNode Class
    let cameraNode = SKCameraNode()
    var accVal : Double = 1
    var xMov : Double = 0
    var yMov : Double = 0
    var isZPressed : Bool = false
    var playerLocArray = [CGPoint]()
    
    //TODO: StatusBarView Class
    var statusBar: SKView = SKView()
    var selectedStatusButton : StatusButtonState = .player
    
    var centeredTilePoint: CGPoint = CGPoint()
    
    //TODO: ObjectMapNode & BackgroundMapNode classes with functions. Need to clean up the logic in game scene
    var objectMap : SKTileMapNode!
    var backgroundMap : SKTileMapNode!
    
    //TODO: HighlightBoxShapeNode Class
    var highlightedBox : SKShapeNode!
    
    var tileSet : SKTileSet!
    
    var currentPlaceableSelected: Placeable?
    
    override func didMove(to view: SKView) {
        retrieveSceneComponents()
    }
    
    func touchDown(atPoint pos : CGPoint) {
        moveHighlight(toPoint: pos)
        updateStatusBar()
    }
    
    func updateStatusBar(){
        statusBar.subviews.filter{($0 is NSScrollView || $0 is DetailView)}.forEach{$0.removeFromSuperview()}
        if selectedStatusButton != .map{
            createObjectMenu()
        }else{
            createMapMenu()
        }
    }
    func createMapMenu(){
        let scrollMenu = NSScrollView(frame: NSRect(x: 60, y: 0, width: (self.view?.bounds.width)! - 60, height: 100))
        
        let currentTileColumn = backgroundMap.tileColumnIndex(fromPosition: centeredTilePoint)
        let currentTileRow = backgroundMap.tileRowIndex(fromPosition: centeredTilePoint)
        let currentTile = backgroundMap.tileGroup(atColumn: currentTileColumn, row: currentTileRow)
        
        let textField1 = NSLabel(frame: NSRect(x: 20, y: 0, width: (self.view?.bounds.width)! - 60, height: 100), text: "Terrain: ")
        let textField2 = NSLabel(frame: NSRect(x: 100, y: 0, width: (self.view?.bounds.width)! - 60, height: 100), text: "None")
        
        if((currentTile) != nil){
            textField2.stringValue = (currentTile?.name)!
        }
        
        scrollMenu.addSubview(textField1)
        scrollMenu.addSubview(textField2)
        statusBar.addSubview(scrollMenu)
    }
    func createObjectMenu(){
        
        let scrollMenu = NSScrollView(frame: NSRect(x: 60, y: 0, width: (self.view?.bounds.width)! - 60, height: 100))

//        getPlaceable(at: centeredTilePoint).filter{ $0 is Placeable}
//
        //create button for each object
        var count = 0
        let objects: [Placeable] = (getPlaceable(at: centeredTilePoint) as! [Placeable]).filter{ $0.placeableType.rawValue == selectedStatusButton.rawValue }
        objects.forEach{
            let button = NSButton(frame: NSRect(x: 80+count*80, y: 20, width: 60, height: 60))
            button.title = $0.nodeName
            button.target = self
            button.action = #selector(GameScene.objectViewButtonTapped)
            scrollMenu.addSubview(button)
            count += 1
        }
        
        let addButton = NSButton(frame: NSRect(x: 10, y: 20, width: 60, height: 60))
        addButton.title = "ADD"
        addButton.tag = 0
        addButton.target = self
        addButton.action = #selector(GameScene.objectViewButtonTapped)
        scrollMenu.addSubview(addButton)
        
        statusBar.addSubview(scrollMenu)
    }
    func objectViewButtonTapped(_ sender: NSButton){
        //if add button pressed, send a new object to the detail view
        if(sender.title == "ADD"){
            guard let type = PlaceableType(rawValue: selectedStatusButton.rawValue) else {
                print("Type: \(selectedStatusButton.rawValue)")
                return
            }
            let newObject: Placeable = createPlaceable(onPoint: centeredTilePoint, placeableType: type)
            currentPlaceableSelected = newObject
            createDetailMenu(object: newObject)
            return
        }
        //if a different button is pressed, get the objects referred to by the button and send it to the detail view
        getPlaceable(at: centeredTilePoint).flatMap{ $0 }.forEach{
            if($0 is RadiusView){
                let o = $0 as! RadiusView
                if(sender.title == o.nodeName){
                    currentPlaceableSelected = o
                    createDetailMenu(object: o)
                }
            }else if ($0 is PlayerView){
                let o = $0 as! PlayerView
                if(sender.title == o.nodeName){
                    currentPlaceableSelected = o
                    createDetailMenu(object: o)
                }
            }else if($0 is MonsterView){
                let o = $0 as! MonsterView
                if(sender.title == o.nodeName){
                    currentPlaceableSelected = o
                    createDetailMenu(object: o)
                }
            }else if($0 is NPCView){
                let o = $0 as! NPCView
                if(sender.title == o.nodeName){
                    currentPlaceableSelected = o
                    createDetailMenu(object: o)
                }
            }else if($0 is ObjectView){
                let o = $0 as! ObjectView
                if(sender.title == o.nodeName){
                    currentPlaceableSelected = o
                    createDetailMenu(object: o)
                }
            }
        }
    }
    func createDetailMenu(object: Placeable){
        //clear statusbar
        statusBar.subviews.filter{($0 is NSScrollView)}.forEach{$0.removeFromSuperview()}
        
        //create detail menu view
        let detailMenuFrame = NSRect(x: 60, y: 0, width: (self.view?.bounds.width)! - 60, height: 100)
        let detailMenu = DetailView(object: object, frame: detailMenuFrame, objectType: selectedStatusButton.rawValue)
        
        //set textfield delegates to the gamescene
        let textFields = detailMenu.subviews.filter{$0 is NSTextField}
        for textField in textFields{
            let nsTextField = textField as! NSTextField
            nsTextField.delegate = self
        }
        
        //set delete button function
        let buttons = detailMenu.subviews.filter{$0 is NSButton}
        for button in buttons{
            let b = button as! NSButton
            if (b.title == "Delete"){
                b.target = self
                b.action = #selector(deleteButtonTapped)
            }
        }
        
        statusBar.addSubview(detailMenu)
    }
    
    func deleteButtonTapped(_ sender: NSButton){
        // delete placeable, get rid of detailView, and go back to objectMenu
        (currentPlaceableSelected as! SKShapeNode).removeFromParent()
        statusBar.subviews.filter{($0 is DetailView)}.forEach{$0.removeFromSuperview()}
        createObjectMenu()
    }
    

    //TODO: createStatusBar -> StatusBarView
    func createStatusBar(){
        guard let width : CGFloat = self.view?.bounds.width else{
            fatalError("width")
        }
        let frame = NSRect(x: 0, y: 0, width: width, height: 100)
        statusBar.frame = frame
        createStatusButton(strings: StatusButtonState.player.rawValue, StatusButtonState.monster.rawValue, StatusButtonState.object.rawValue, StatusButtonState.npc.rawValue, StatusButtonState.map.rawValue, StatusButtonState.radius.rawValue).forEach{
            statusBar.addSubview($0)
        }
        
        //player is default selected status button, so set the border as selected
        statusBar.subviews.filter{$0 is NSButton}.forEach{
            let button: NSButton = ($0 as! NSButton)
            if (button.title == PlaceableType.player.rawValue){
                button.setBorder(color: .orange, width: 2)
            }
        }
        
        self.view?.addSubview(statusBar)
    }
    private func createStatusButton(strings: String...) -> [NSButton] {
        let height = statusBar.frame.height / CGFloat(strings.count)
        var currentY: CGFloat = 0
        var tag = 1
        return strings.map {
            let button = NSButton(frame: NSRect(x: 0, y: currentY, width: 60, height: height))
            button.title = $0
            button.tag = tag
            button.target = self
            button.action = #selector(GameScene.statusButtonTapped)
            tag += 1
            currentY += height
            return button
        }
    }
    func statusButtonTapped(_ sender: NSButton){
        let buttons = statusBar.subviews.filter{$0 is NSButton}.filter{$0.tag > 0}
        for button in buttons{
            button.setBorder(color: .black, width: 0)
        }
        
        sender.setBorder(color: .orange, width: 2)
        
        switch(sender.tag){
        case 1:
            selectedStatusButton = .player
        case 2:
            selectedStatusButton = .monster
        case 3:
            selectedStatusButton = .object
        case 4:
            selectedStatusButton = .npc
        case 5:
            selectedStatusButton = .map
        case 6:
            selectedStatusButton = .radius
        default:
            return
        }
        updateStatusBar()
    }
    
    
    func touchMoved(toPoint pos : CGPoint) {
    }
    func touchUp(atPoint pos : CGPoint) {
    }
    override func mouseDown(with event: NSEvent) {
        self.touchDown(atPoint: event.location(in: self))
    }
    override func mouseDragged(with event: NSEvent) {
        self.touchMoved(toPoint: event.location(in: self))
    }
    
    override func mouseUp(with event: NSEvent) {
        self.touchUp(atPoint: event.location(in: self))
    }
    
    override func keyDown(with event: NSEvent) {
        if(event.keyCode == 6){
            isZPressed = true
        }
        
        if(event.keyCode == 123){
            //("Left Arrow")
            xMov = -100
            if accVal < 3{
                accVal = accVal*1.2
            }
        }
        
        if(event.keyCode == 124){
            //print("Right Arrow")
            xMov = 100
            if accVal < 3{
                accVal = accVal*1.2
            }
        }
        
        if(event.keyCode == 125){
            //print("Bottom Arrow")
            yMov = -100
            if isZPressed {
                if (camera?.xScale)! + 1 < 7{
                    camera?.xScale = ((camera?.xScale)!+1)
                    camera?.yScale = ((camera?.yScale)!+1)
                }
                return
            }
            if accVal < 3{
                accVal = accVal*1.2
            }
        }
        
        if(event.keyCode == 126){
            //print("Up Arrow")
            yMov = 100
            if isZPressed {
                if (camera?.xScale)! - 1 > 0  {
                    camera?.xScale = ((camera?.xScale)!-1)
                    camera?.yScale = ((camera?.yScale)!-1)
                }
                return
            }
            if accVal < 3{
                accVal = accVal*1.2
            }
        }
        camera?.position = CGPoint(x: (camera?.position.x)! + CGFloat(xMov*accVal), y: (camera?.position.y)! + CGFloat(yMov*accVal))
    }
    
    override func keyUp(with event: NSEvent) {
        switch event.keyCode {
        case 123, 124:
            accVal = 1
            xMov = 0
        case 125, 126:
            accVal = 1
            yMov = 0
        case 6:
            isZPressed = false
        default :
            print("")
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    func retrieveSceneComponents(){
        guard let scene = SKScene(fileNamed: "TileTest") else{
            return
        }
        guard let tileSet = SKTileSet(named: "Tiles") else {
            return
        }
        guard let backgroundMap = scene.childNode(withName: "BackgroundMap") as! SKTileMapNode? else{
            return
        }
        guard let objectMap = scene.childNode(withName: "ObjectMap") as! SKTileMapNode? else{
            return
        }
        
        self.tileSet = tileSet
        self.backgroundMap = backgroundMap
        self.objectMap = objectMap
        
        camera = objectMap.childNode(withName: "dmCamera") as! SKCameraNode!
        camera?.position = CGPoint(x: 0, y: 0)
        camera?.xScale = 5
        camera?.yScale = 5
        
        createHighlight()
        createStatusBar()
    }
    
    //TODO: createHighlight -> HighlightBoxShapeNode
    func createHighlight(){
        let size : CGSize = objectMap.tileSize
        highlightedBox = SKShapeNode(rect: CGRect(x: 0, y: 0, width: size.width+1, height: size.height+1))
        highlightedBox.lineWidth = 20
        highlightedBox.isHidden = true
        highlightedBox.strokeColor = .orange
        addChild(highlightedBox)
    }
    
    //TODO: moveHighlight -> HighlightBoxShapeNode
    func moveHighlight(toPoint: CGPoint){
        let column = objectMap.tileColumnIndex(fromPosition: toPoint)
        let row = objectMap.tileRowIndex(fromPosition: toPoint)
        let centerTilePoint = objectMap.centerOfTile(atColumn: column, row: row)
        centeredTilePoint = centerTilePoint
        highlightedBox.isHidden = false
        highlightedBox.zPosition = LayerPosition.radiusLayer.rawValue
        highlightedBox.position = CGPoint(x: (centerTilePoint.x - highlightedBox.frame.width / 2)+11, y: (centerTilePoint.y - highlightedBox.frame.height / 2)+11)
        
    }
    
    func getTileOrigin(atPoint: CGPoint)->CGPoint{
        let column = objectMap.tileColumnIndex(fromPosition: atPoint)
        let row = objectMap.tileRowIndex(fromPosition: atPoint)
        var point = objectMap.centerOfTile(atColumn: column, row: row)
        point.x -= objectMap.tileSize.width/2
        point.y -= objectMap.tileSize.height/2
        return point
    }

    
    func createPlaceable(onPoint pos: CGPoint, placeableType: PlaceableType)->Placeable{
        var placeable : Placeable
        switch(placeableType){
        case .player:
            placeable = createPlayer(Name: "zach", Size: CGSize( width: 128, height: 128), Color: .blue, Position: pos, Health: 10, CurrentHealth: 10, Level: 1, Xp: 1, Initiative: 1, Ac: 1, Speed: 1, Stats: Stats( strength: 1, dexterity: 1, constitution: 1, intelligence: 1, wisdom: 1, charisma: 1), Currency: Currency(pp: 0, gp: 0, ep: 0, sp: 0, cp: 0))
            placeable.color = .blue
        case .monster:
            placeable = createMonster(Name: "Monster", Size: CGSize( width: 128, height: 128), Color: .green, Position: pos, Health: 10, CurrentHealth: 10, Level: 1, Xp: 1, Initiative: 1, Ac: 1, Speed: 1, Stats: Stats( strength: 1, dexterity: 1, constitution: 1, intelligence: 1, wisdom: 1, charisma: 1), Currency: Currency(pp: 0, gp: 0, ep: 0, sp: 0, cp: 0))
            placeable.color = .green
        case .npc:
            placeable = createNPC(Name: "Villager", Size: CGSize( width: 128, height: 128), Color: .brown, Position: pos, Health: 1, CurrentHealth: 1, Currency: Currency(pp: 0, gp: 0, ep: 0, sp: 0, cp: 0))
            placeable.color = .brown
        case .object:
            placeable = createObject(Name: "Chest", Size: CGSize( width: 128, height: 128), Color: .yellow, Position: pos, Currency: Currency(pp: 0, gp: 0, ep: 0, sp: 0, cp: 0))
            placeable.color = .yellow
        case .radius:
            placeable = createRadius(Name: "Radius", Size: CGSize( width: 128, height: 128), Color: .red, Position: pos)
            placeable.color = .red
        }
        return placeable
    }
    func createRadius(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint)->RadiusView{
        let radiusPosition = getTileOrigin(atPoint: position)
        let radius = RadiusView(Name: name, Size: size, Color: color, Position: radiusPosition)
        radius.zPosition = LayerPosition.radiusLayer.rawValue
        addChild(radius)
        return radius
    }
    //Used from saved players 
    //TODO: Take in a player object?
    //          createPlayer -> addPlayerToGame(pos) -> loadPlayerToGame(playerView)
    private func createPlayer(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Level level: Int, Xp xp: Int, Initiative initiative: Int, Ac ac: Int, Speed speed: Int, Stats stats: Stats, Currency currency: Currency)->PlayerView{
        let playerPosition = getTileOrigin(atPoint: position)
        playerLocArray.append(playerPosition)
        let player = PlayerView(Name: name, Size: size, Color: color, Position: playerPosition, Health: health, CurrentHealth: currentHealth, Level: level, Xp: xp, Initiative: initiative, Ac: ac, Speed: speed, Stats: stats, Currency: currency)
        player.zPosition = LayerPosition.placeableLayer.rawValue
        addChild(player)
        return player
    }
    private func createPlayer()->PlayerView{
        let playerPosition = getTileOrigin(atPoint: position)
        playerLocArray.append(playerPosition)
        let player = PlayerView(position: playerPosition)
        player.zPosition = LayerPosition.placeableLayer.rawValue
        addChild(player)
        return player
    }
    func createObject(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Currency currency: Currency)->ObjectView{
        let objectPosition = getTileOrigin(atPoint: position)
        let object = ObjectView(Name: name, Size: size, Color: color, Position: objectPosition, Currency: currency)
        object.zPosition = LayerPosition.placeableLayer.rawValue
        addChild(object)
        return object
    }
    func createMonster(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Level level: Int, Xp xp: Int, Initiative initiative: Int, Ac ac: Int, Speed speed: Int, Stats stats: Stats, Currency currency: Currency)->MonsterView{
        let monsterPosition = getTileOrigin(atPoint: position)
        let monster = MonsterView(Name: name, Size: size, Color: color, Position: monsterPosition, Health: health, CurrentHealth: currentHealth, Level: level, Xp: xp, Initiative: initiative, Ac: ac, Speed: speed, Stats: stats, Currency: currency)
        monster.zPosition = LayerPosition.placeableLayer.rawValue
        addChild(monster)
        return monster
    }
    func createNPC(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint, Health health: Int, CurrentHealth currentHealth: Int, Currency currency: Currency)->NPCView{
        let npcPosition = getTileOrigin(atPoint: position)
        let npc = NPCView(Name: name, Size: size, Color: color, Position: npcPosition, Health: health, CurrentHealth: currentHealth, Currency: currency)
        npc.zPosition = LayerPosition.placeableLayer.rawValue
        addChild(npc)
        return npc
    }
    
    func getPlaceable(at: CGPoint) -> [Placeable?]{
        let placeables = self.children.filter{$0 is Placeable}.filter{ $0.contains(at) }.map{$0 as! Placeable}
        var objects : [Placeable] = []
        for p in placeables{
            if (p is PlayerView){
                let o = p as! PlayerView
                objects.append(o)
            }else if(p is MonsterView){
                let o = p as! MonsterView
                objects.append(o)
            }else if(p is NPCView){
                let o = p as! NPCView
                objects.append(o)
            }else if(p is ObjectView){
                let o = p as! ObjectView
                objects.append(o)
            }
        }
        
        return placeables
    }
    
    override func controlTextDidChange(_ obj: Notification) {
    }
    override func controlTextDidEndEditing(_ obj: Notification) {
        let textField = obj.object as! NSTextField
        changePlaceable(string: textField.stringValue, tag: textField.tag, object: currentPlaceableSelected!)
    }

    override func controlTextDidBeginEditing(_ obj: Notification) {
    }

    func isPlayerPresent(atLoc pos: CGPoint)->Bool{
        for loc in playerLocArray {
            if (getTileOrigin(atPoint: pos) == loc) {
                return true
            }
        }
        return false
    }
}
