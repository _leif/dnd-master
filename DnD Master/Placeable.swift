//
//  Placeable.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 3/4/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit

protocol Placeable{//Nodes
    var nodeName : String {get set}
    var size : CGSize {get set}
    var color : SKColor? {get set}
    var placeableType: PlaceableType {get set}
}
//NodeModels
protocol Living{
    var health : Int {get set}
    var currentHealth : Int {get set}
}
protocol Levelable{
    var level : Int {get set}
    var xp : Int {get set}
}
protocol Fightable{
    var initiative : Int {get set}
    var ac : Int {get set}
    var speed : Int {get set}
    var stats : Stats {get set}
}
protocol Carryable{
    var currency : Currency {get set}
}
struct Stats {
    var strength : Int
    var dexterity : Int
    var constitution : Int
    var intelligence : Int
    var wisdom : Int
    var charisma : Int
}
struct Currency{
    var pp : Int
    var gp : Int
    var ep : Int
    var sp : Int
    var cp : Int
}
