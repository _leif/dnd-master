//
//  RadiusView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 4/21/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit
//user input and functions
//prepare to display

class RadiusView: SKShapeNode, Placeable{
    var placeableType: PlaceableType = PlaceableType.radius
    var nodeName : String
    var size : CGSize {
        didSet {
            let newFrame = CGRect(x: 64 - size.width/2, y: 64 - size.height/2, width: size.width, height: size.height)
            path = CGPath(ellipseIn: newFrame, transform: nil)
        }
    }
    var color : SKColor? {
        didSet {
            strokeColor = color!
            lineWidth = 10
        }
    }
    
    init(Name name: String, Size size: CGSize, Color color: SKColor, Position position: CGPoint) {
        nodeName = name
        self.size = size
        super.init()
        self.color = color
        path = CGPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerWidth: size.width/2, cornerHeight: size.height/2, transform: nil)
        
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
