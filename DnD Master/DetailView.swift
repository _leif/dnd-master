//
//  DetailView.swift
//  DnD Master
//
//  Created by Sabien Ambrose on 4/3/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation
import SpriteKit

class DetailView: SKView{
    var objectPassed: Placeable
    var objectPassedType: String
    
    init(object: Placeable, frame: NSRect, objectType: String){
        objectPassed = object
        objectPassedType = objectType
        super.init(frame: frame)
        
        createDeleteButton()
        
        switch objectType{
        case PlaceableType.player.rawValue:
            createHPField()
            createLabels(strings: ["name","color"], y: 80) //placeable
            createLabels(strings: ["level", "xp", "initiative", "speed", "ac"], y: 60)
            createLabels(strings: ["str", "con", "dex", "int", "wis", "cha"], y: 40)
            createLabels(strings: ["pp", "gp", "ep", "sp", "cp"], y: 20)
        case PlaceableType.monster.rawValue:
            createHPField()
            createLabels(strings: ["name","color"], y: 80)
            createLabels(strings: ["level", "xp", "initiative", "speed","ac"], y: 60)
            createLabels(strings: ["str", "con", "dex", "int", "wis", "cha"], y: 40)
            createLabels(strings: ["pp", "gp", "ep", "sp", "cp"], y: 20)
        case PlaceableType.npc.rawValue:
            createHPField()
            createLabels(strings: ["name","color"], y: 80)
            createLabels(strings: ["pp", "gp", "ep", "sp", "cp"], y: 20)
        case PlaceableType.object.rawValue:
            createLabels(strings: ["name","color"], y: 80)
            createLabels(strings: ["pp", "gp", "ep", "sp", "cp"], y: 20)
        case PlaceableType.radius.rawValue:
            createLabels(strings: ["name","color", "size"], y: 80)
        default:
            return
        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func createDeleteButton(){
        let deleteButton = NSButton(frame: NSRect(x: self.frame.width-60, y: 80, width: 60, height: 20))
        deleteButton.title = "Delete"
        addSubview(deleteButton)
    }
    func createHPField(){
        let label = NSLabel(frame: NSRect( x: 230, y: 80, width: 30, height: 20), text: "HP:")
        let field = NSTextField(frame: NSRect(x: 260, y: 80, width: 30, height: 20))
        let slashLabel = NSLabel(frame: NSRect(x: 290, y: 80, width: 10, height: 20), text: "/")
        let field2 = NSTextField(frame: NSRect( x: 300, y: 80, width: 30, height: 20))
        
        label.tag = -1
        slashLabel.tag = -1
        field2.tag = TextFieldTag.HP.rawValue
        field.tag = TextFieldTag.currentHP.rawValue
        
        addSubview(label)
        addSubview(field)
        addSubview(slashLabel)
        addSubview(field2)
        
        switch objectPassedType{
        case PlaceableType.player.rawValue:
            let objectAsType = objectPassed as! PlayerView
            field.stringValue = String(objectAsType.playerModel.currentHealth)
            field2.stringValue = String(objectAsType.playerModel.health)
        case PlaceableType.monster.rawValue:
            let objectAsType = objectPassed as! MonsterView
            field.stringValue = String(objectAsType.monsterModel.currentHealth)
            field2.stringValue = String(objectAsType.monsterModel.health)
        case PlaceableType.npc.rawValue:
            let objectAsType = objectPassed as! NPCView
            field.stringValue = String(objectAsType.npcModel.currentHealth)
            field2.stringValue = String(objectAsType.npcModel.health)
        default:
            return
        }
        
        
        
    }
    func createLabels(strings: [String], y: CGFloat){
        var currentX: CGFloat = 0
        strings.forEach{
            let label = NSLabel(frame: NSRect( x: currentX, y: y, width: 45, height: 20), text: $0 + ":")
            let field = NSTextField(frame: NSRect( x: currentX + label.frame.width, y: y, width: 60, height: 20))
            
            label.tag = -1
            
            addSubview(label)
            addSubview(field)
            
            currentX += 110
            //check the string, for each one set the placeholder string
            switch $0{
            case PlaceableAttributes.size.rawValue:
                field.tag = 0
                let objectAsType = objectPassed as! RadiusView
                let size : String = (objectAsType.size.width / 128).description
                field.stringValue = size
            case PlaceableAttributes.name.rawValue:
                field.tag = TextFieldTag.name.rawValue
                field.stringValue = objectPassed.nodeName
            case PlaceableAttributes.color.rawValue:
                field.tag = TextFieldTag.color.rawValue
                field.stringValue = (objectPassed.color?.toHexString())!
            case PlaceableAttributes.level.rawValue:
                field.tag = TextFieldTag.level.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.level)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.level)
                default:
                    return
                }
            case PlaceableAttributes.xp.rawValue:
                field.tag = TextFieldTag.xp.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.xp)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.xp)
                default:
                    return
                }
            case PlaceableAttributes.speed.rawValue:
                field.tag = TextFieldTag.speed.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.speed)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.speed)
                default:
                    return
                }
            case PlaceableAttributes.ac.rawValue:
                field.tag = TextFieldTag.ac.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.ac)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.ac)
                default:
                    return
                }
            case PlaceableAttributes.inititative.rawValue:
                field.tag = TextFieldTag.inititative.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.initiative)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.initiative)
                default:
                    return
                }
            case PlaceableAttributes.str.rawValue:
                field.tag = TextFieldTag.str.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.strength)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.strength)
                default:
                    return
                }
            case PlaceableAttributes.con.rawValue:
                field.tag = TextFieldTag.con.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.constitution)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.constitution)
                default:
                    return
                }
            case PlaceableAttributes.dex.rawValue:
                field.tag = TextFieldTag.dex.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.dexterity)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.dexterity)
                default:
                    return
                }
            case PlaceableAttributes.int.rawValue:
                field.tag = TextFieldTag.int.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.intelligence)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.intelligence)
                default:
                    return
                }
            case PlaceableAttributes.wis.rawValue:
                field.tag = TextFieldTag.wis.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.wisdom)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.wisdom)
                default:
                    return
                }
            case PlaceableAttributes.cha.rawValue:
                field.tag = TextFieldTag.cha.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.stats.charisma)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.stats.charisma)
                default:
                    return
                }
            case PlaceableAttributes.pp.rawValue:
                field.tag = TextFieldTag.pp.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.currency.pp)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.currency.pp)
                case PlaceableType.npc.rawValue:
                    let objectAsType = objectPassed as! NPCView
                    field.stringValue = String(objectAsType.npcModel.currency.pp)
                case PlaceableType.object.rawValue:
                    let objectAsType = objectPassed as! ObjectView
                    field.stringValue = String(objectAsType.objectModel.currency.pp)
                default:
                    return
                }
            case PlaceableAttributes.gp.rawValue:
                field.tag = TextFieldTag.gp.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.currency.gp)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.currency.gp)
                case PlaceableType.npc.rawValue:
                    let objectAsType = objectPassed as! NPCView
                    field.stringValue = String(objectAsType.npcModel.currency.gp)
                case PlaceableType.object.rawValue:
                    let objectAsType = objectPassed as! ObjectView
                    field.stringValue = String(objectAsType.objectModel.currency.gp)
                default:
                    return
                }
            case PlaceableAttributes.ep.rawValue:
                field.tag = TextFieldTag.ep.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.currency.ep)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.currency.ep)
                case PlaceableType.npc.rawValue:
                    let objectAsType = objectPassed as! NPCView
                    field.stringValue = String(objectAsType.npcModel.currency.ep)
                case PlaceableType.object.rawValue:
                    let objectAsType = objectPassed as! ObjectView
                    field.stringValue = String(objectAsType.objectModel.currency.ep)
                default:
                    return
                }
            case PlaceableAttributes.sp.rawValue:
                field.tag = TextFieldTag.sp.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.currency.sp)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.currency.sp)
                case PlaceableType.npc.rawValue:
                    let objectAsType = objectPassed as! NPCView
                    field.stringValue = String(objectAsType.npcModel.currency.sp)
                case PlaceableType.object.rawValue:
                    let objectAsType = objectPassed as! ObjectView
                    field.stringValue = String(objectAsType.objectModel.currency.sp)
                default:
                    return
                }
            case PlaceableAttributes.cp.rawValue:
                field.tag = TextFieldTag.cp.rawValue
                switch objectPassedType{
                case PlaceableType.player.rawValue:
                    let objectAsType = objectPassed as! PlayerView
                    field.stringValue = String(objectAsType.playerModel.currency.cp)
                case PlaceableType.monster.rawValue:
                    let objectAsType = objectPassed as! MonsterView
                    field.stringValue = String(objectAsType.monsterModel.currency.cp)
                case PlaceableType.npc.rawValue:
                    let objectAsType = objectPassed as! NPCView
                    field.stringValue = String(objectAsType.npcModel.currency.cp)
                case PlaceableType.object.rawValue:
                    let objectAsType = objectPassed as! ObjectView
                    field.stringValue = String(objectAsType.objectModel.currency.cp)
                default:
                    return
                }
            default:
                return
            }
        }
    }
}
