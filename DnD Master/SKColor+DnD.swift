//
//  SKColor+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 4/26/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import SpriteKit

extension SKColor{
    public class func hexColor(rgbValue: Int, alpha: CGFloat = 1.0) -> NSColor {
        return NSColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0, green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0, blue:((CGFloat)(rgbValue & 0xFF))/255.0, alpha:alpha)
    }
    public func toHexString() -> String {
        let r = Int(redComponent * 255)
        let g = Int(greenComponent * 255)
        let b = Int(blueComponent * 255)
        
        var red = String(format: "%2X", r)
        if (red == " 0"){
            red = "00"
        }
        
        var green = String(format: "%2X", g)
        if (green == " 0"){
            green = "00"
        }
        
        var blue = String(format: "%2X", b)
        if (blue == " 0"){
            blue = "00"
        }
        
        let hexString = red+green+blue
        Swift.print("red: \(red) green: \(green) blue: \(blue)")
        return hexString
    }
}
