//
//  String+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 4/26/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation

//if the string has a number, return it, else return 0
extension String {
    func toInt(defaultInt: Int) -> Int {
        if let result = Int(self) {
            return result
        }
        else {
            return defaultInt
        }
    }
}
