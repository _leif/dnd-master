//
//  Enum+DnD.swift
//  DnD Master
//
//  Created by Zach Eriksen on 4/26/17.
//  Copyright © 2017 oneleif. All rights reserved.
//

import Foundation

enum LayerPosition: CGFloat {
    case radiusLayer = 15
    case placeableLayer = 10
}

enum PlaceableType: String{
    case player = "player"
    case monster = "monster"
    case npc = "npc"
    case object = "object"
    case radius = "radius"
    
}
enum StatusButtonState: String{
    case player = "player"
    case monster = "monster"
    case npc = "npc"
    case object = "object"
    case map = "map"
    case radius = "radius"
}

enum PlaceableAttributes: String{
    case size = "size"
    case name = "name"
    case color = "color"
    case currentHP = "currentHP"
    case HP = "HP"
    case level = "level"
    case xp = "xp"
    case speed = "speed"
    case ac = "ac"
    case inititative = "initiative"
    case str = "str"
    case con = "con"
    case dex = "dex"
    case int = "int"
    case wis = "wis"
    case cha = "cha"
    case pp = "pp"
    case gp = "gp"
    case ep = "ep"
    case sp = "sp"
    case cp = "cp"
}

enum TextFieldTag: Int{
    case size = 0
    case name = 1
    case color = 2
    case currentHP = 3
    case HP = 4
    case level = 5
    case xp = 6
    case speed = 7
    case ac = 8
    case inititative = 9
    case str = 10
    case con = 11
    case dex = 12
    case int = 13
    case wis = 14
    case cha = 15
    case pp = 16
    case gp = 17
    case ep = 18
    case sp = 19
    case cp = 20
}

enum CurrentStatusView{
    case objectView, detailView
}
